import groovy.sql.Sql
import spock.lang.Shared
import spock.lang.Specification
//jms
import com.tibco.tibjms.TibjmsConnectionFactory

import javax.jms.Connection;
import javax.jms.ConnectionFactory
import javax.jms.ExceptionListener
import javax.jms.JMSException
import javax.jms.MessageConsumer
import javax.jms.Message
import javax.jms.Session
import javax.jms.Queue

class EmsSpec extends Specification {
    @Shared ConnectionFactory jmsFactory = new TibjmsConnectionFactory("tcp://localhost:7222"){{setUserName("admin");setUserPassword("");}}
    @Shared Connection conexionJMS
    @Shared Session sesionJMS


    def setupSpec() {
        //conexión al jms
        conexionJMS = jmsFactory.createConnection();
        //activamos la conexión para lectura
        conexionJMS.clientID = "spock:${System.currentTimeMillis()}";
        conexionJMS.exceptionListener = {JMSException e -> logger.error("JMS Exception", e)} as ExceptionListener;
        //creamos la sesión para escritura
        sesionJMS = conexionJMS.createSession(false, Session.AUTO_ACKNOWLEDGE);
        conexionJMS.start()
    }

    def cleanupSpec(){
        sesionJMS.close()
        conexionJMS.stop()
    }


    def "invocaciones diferentes para mensaje a"(){

        expect:
        mensajeA (params) == result

        where:
        params               | result
        null                 | "hola a todos a es c y b es null luego estan x=0"
        [b:'lorena', l:4]    | "hola a todos a es c y b es lorena luego estan x=0"
        [c:'2+2',g:'4']      | "hola a todos a es c y b es null luego estan 2+2=4"
        [a:'X','b':'Y']      | "hola a todos a es X y b es Y luego estan x=0"
    }

    def mensajeA (flags){
        def defv = {literal, defvalue = '' ->
            return literal == null ? defvalue : literal
        }
        return "hola a todos a es ${defv (flags?.a,'c')} y b es ${flags?.b} luego estan ${defv flags?.c,'x'}=${defv flags?.g,0}"
    }

    def "escritura en cola cualquiera"(){

        given:
        def cola = sesionJMS.createQueue("colaloa")
        def texto = "hola"

        when: //escribimo el mensaje en una cola
        sesionJMS.createProducer(cola).send(sesionJMS.createTextMessage(texto));

        and: //leemos la cola
        Message mensaje = sesionJMS.createConsumer(cola).receive(10000)

        then:
        notThrown(Exception) //no ha habido errores
        texto == mensaje?.text // el mensaje era el enviado

        cleanup://valicar colas
        for(Message chunk = mensaje; chunk = sesionJMS.createConsumer(cola).receiveNoWait() ; chunk != null){}
    }
}