package becletest

import groovy.util.slurpersupport.GPathResult
import spock.lang.IgnoreRest
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Timeout
import tools.jms.EmsSourceImp
import tools.jms.JmsInterface
import tools.xml.XsdValidator

import javax.jms.*

class IntegratedBasicSpec extends Specification {
    //variables compartidas por todas las pruebas
    @Shared jmsTimeout = 3000
    @Shared JmsInterface jms = new EmsSourceImp("tcp://localhost:7222","admin","")
    @Shared XsdValidator validator

    //variables inicializadas para cada prueba
    def projectInput    = jms.createQueue("ta")
    def projectOutput   = jms.createQueue("tb")
    def auditoria       = jms.createQueue("GLB.BBVA.ARC.DE.CLE.CLEMESSAGE.INSERT")
    def notificacion    = jms.createQueue("GLB.BBVA.ARC.DE.KYGN.NOTIFICATION.SEND")
    def notificacionRsp = jms.createQueue("GLB.BBVA.ARC.DE.KYGN.NOTIFICATION.REPLY")
    def epub            = jms.createQueue("tepub")
    def ipub            = jms.createQueue("tipub")
    def purgableQueue   = [projectInput, projectOutput, auditoria, notificacion, notificacionRsp, epub]
    def testUUIDbared =  java.util.UUID.randomUUID().toString().replace("-","") // de esta forma: f7c8607839b346aaab4cf4bdd49958f

    //inicializacion del conjunto de pruebas
    def setupSpec() {
        jms.timeout = jmsTimeout
        jms.connect()
        validator = new XsdValidator(logOnError: true)
        validator.addXSDFolder new File('../TIBCO_BE_CLE/Architecture/SharedResources')
    }

    //construcción de cada prueba
    def setup (){
        jms.purgeQueue(purgableQueue)
    }

    //destrucci�n dle conjunto de pruebas
    def cleanupSpec(){
        jms.disconnect()
    }

    //////////////////////////////////////////////////////// datos /////////////////////////////////////////////
    private String createBasicInput(String body) {
        def baseArchitectureHeader = """
            <ns0:Message xmlns:ns0="http://www.bbva.com/EAIFramework">
               <ns0:ArchitectureHeader>
                  <ns0:AuditHeader>
                     <ns0:E2EId>11111111222233334444555555555555</ns0:E2EId>
                     <ns0:ParentInstanceId>23141111222233334444555555555555</ns0:ParentInstanceId>
                     <ns0:InstanceId>${testUUIDbared}</ns0:InstanceId>
                     <ns0:Deepness>0</ns0:Deepness>
                     <ns0:ServiceId>S_KYGN_CA_SEND_PLAN</ns0:ServiceId>
                     <ns0:ServiceType>CHAA</ns0:ServiceType>
                     <ns0:E2EControl>false</ns0:E2EControl>
                     <ns0:ParentE2EControl>true</ns0:ParentE2EControl>
                  </ns0:AuditHeader>
                  <ns0:RequestHeader>
                     <ns0:BBVAUser>BBVAUser</ns0:BBVAUser>
                     <ns0:ExternalId>456</ns0:ExternalId>
                     <ns0:BatchId>567</ns0:BatchId>
                     <ns0:BusinessObject>BusinessObject</ns0:BusinessObject>
                     <ns0:StartTimestamp>2014-01-28T01:37:00.155Z</ns0:StartTimestamp>
                     <ns0:ServiceIdStarter>S_KYGN_CA_SEND_PLAN</ns0:ServiceIdStarter>
                  </ns0:RequestHeader>
                  <ns0:ReprocessingHeader>
                     <ns0:RetriesNumber>2</ns0:RetriesNumber>
                     <ns0:OriginalInstanceId>21289123782128912378212891237821</ns0:OriginalInstanceId>
                  </ns0:ReprocessingHeader>
                  <ns0:SecurityHeader>
                     <ns0:User>suser</ns0:User>
                     <ns0:Password>spass</ns0:Password>
                  </ns0:SecurityHeader>
                  <ns0:ErrorHeader>
                     <ns0:IsError>false</ns0:IsError>
                  </ns0:ErrorHeader>
               </ns0:ArchitectureHeader>
               <ns0:Body>
                  ${body}
               </ns0:Body>
            </ns0:Message> """
    }

    /** creación de una respuesta de notificación, con error y descripción parametrizables */
    private String CreateNotificationResponse(String errorCode, String errorDesc) {
        """<ns:Envelope xmlns:ns="http://schemas.xmlsoap.org/soap/envelope/">
               <ns:Header/>
               <ns:Body>
                   <ns0:sendNotificationResponse xmlns:ns0="http://bbva.com/es/bbva/cib/types/sendNotificationResponseMsg/V1.0">
                       <ns0:ErrorCode>${errorCode}</ns0:ErrorCode>
                       <ns0:ErrorDesc>${errorDesc}</ns0:ErrorDesc>
                       <!--Optional:-->
                       <ns0:IdNotificacion>2332</ns0:IdNotificacion>
                   </ns0:sendNotificationResponse>
               </ns:Body>
            </ns:Envelope>"""
    }

    /** verifica un evento sin tener en cuenta todos sus datos */
    private void assertSimpleAuditMessage(Message auditEvent, String eventCode,String serviceId, String instanceId, String procesName) {
        assert auditEvent != null;
        assert validator.validate(auditEvent?.text).valid
        GPathResult beanAudit = new XmlSlurper().parseText(auditEvent?.text)
        assert eventCode == beanAudit.CLEHeader.EventCode.text()
        assert serviceId == beanAudit.CLEHeader.ServiceId.text()
        assert procesName == beanAudit.CLEHeader.ProcessName.text()
        //    assert instanceId == beanAudit.CLEHeader.InstanceId.text()
    }

    //////////////////////////////////////////////////////// escenarios /////////////////////////////////////////////
    @Timeout (value=6000)
    def "prueba de ejecucion escenario elemental"(){
        given:
        def initMessage = createBasicInput("""
                  <ns1:FunctionalPayload xmlns:ns1="http://tests.soap/be/audit/functionalPayload">
                     <ns1:A>A data</ns1:A>
                     <ns1:B>B data</ns1:B>
                     <ns1:scenarioParam key="scenario" value="base">param1</ns1:scenarioParam>
                     <ns1:scenarioParam key="serviceConfig" value="E2EControl">${QE2ECtrl}</ns1:scenarioParam>
                     <ns1:scenarioParam key="optionalFuncionaHeader" value="ON">${QE2ECtrl}</ns1:scenarioParam>
                  </ns1:FunctionalPayload>
            """);
        def functionalPayloadMark="theFuncPayload"

        when: //peticion basica
        jms.writeQueue(projectInput,initMessage)
        Message auditStart = jms.readQueue(auditoria, jmsTimeout)

        and: //respuesta
        Message auditStop = jms.readQueue(auditoria, jmsTimeout)
        Message respuesta = jms.readQueue(projectOutput,jmsTimeout)
        //sesionJMS.createProducer(gateway).send(sesionJMS.createTextMessage(normalizacionAlienResponse)); //este mensaje debería de no ser cogido por la maquina de estados
        //sesionJMS.createProducer(projectOutput).send(sesionJMS.createTextMessage(normalizacionResponse));


        then:
        notThrown(Exception) //no ha habido errores

        auditStart != null;
        validator.validate(auditStart?.text).errors == []
        def beanAuditStart = new XmlSlurper().parseText(auditStart?.text)
        '11111111222233334444555555555555' == beanAuditStart.CLEHeader.E2EId.text()
        '0'   == beanAuditStart.CLEHeader.E2EDeepness.text()
        'BBVAUser' == beanAuditStart.CLEHeader.InvocationUser.text()
        '567' == beanAuditStart.CLEHeader.BatchId.text()
        'AUD-ARC-0001' == beanAuditStart.CLEHeader.EventCode.text() //initial
        '1' == beanAuditStart.CLEHeader.EventType.text() //audit
        1 == beanAuditStart.CLEHeader.Timestamp.size()
        'S_KYGN_CA_SEND_PLAN' == beanAuditStart.CLEHeader.ServiceId.text()
        //  testUUIDbared == beanAuditStart.CLEHeader.InstanceId.text()
        'TIBCO_BE_CLE_TESTSAMPLE' == beanAuditStart.CLEHeader.ProjectName.text()
        'clebetest' == beanAuditStart.CLEHeader.ProcessName.text()
        1 == beanAuditStart.CLEHeader.EngineName.size()
        1 == beanAuditStart.CLEHeader.MachineName.size()
        beanAuditStart?.Body  != null
        1 == beanAuditStart.Body.size()


        respuesta != null;
        validator.validate(respuesta?.text).errors == []
        def beanRespuesta = new XmlSlurper().parseText(respuesta?.text)
        "theFuncPayload" == beanRespuesta?.Body?.AltPayload?.Text?.toString()
        1 == beanRespuesta.ArchitectureHeader.AuditHeader.E2EId.size()
        "11111111222233334444555555555555" == beanRespuesta.ArchitectureHeader.AuditHeader.E2EId.text()
        32 == beanRespuesta.ArchitectureHeader.AuditHeader.ParentInstanceId.text().size()
        "0" == beanRespuesta?.ArchitectureHeader?.AuditHeader?.Deepness?.toString()
        "S_KYGN_CA_SEND_PLAN"== beanRespuesta?.ArchitectureHeader?.AuditHeader?.ServiceId?.toString()
        "ORCH"== beanRespuesta?.ArchitectureHeader?.AuditHeader?.ServiceType?.toString()
        "false" == beanRespuesta?.ArchitectureHeader?.AuditHeader?.E2EControl?.toString()
        "false" == beanRespuesta?.ArchitectureHeader?.AuditHeader?.ParentE2EControl?.toString()
        "BBVAUser" == beanRespuesta?.ArchitectureHeader?.RequestHeader?.BBVAUser?.toString()
        "456" == beanRespuesta.ArchitectureHeader.RequestHeader.ExternalId.text()
        "567" == beanRespuesta?.ArchitectureHeader?.RequestHeader?.BatchId?.toString()
        "BusinessObject"== beanRespuesta?.ArchitectureHeader?.RequestHeader?.BusinessObject?.toString()
        "2014-01-28T01:37:00.155Z" == beanRespuesta?.ArchitectureHeader?.RequestHeader?.StartTimestamp?.toString()
        "S_KYGN_CA_SEND_PLAN" == beanRespuesta?.ArchitectureHeader?.RequestHeader?.ServiceIdStarter?.toString()
        "0" == beanRespuesta?.ArchitectureHeader?.ErrorHeader?.IsError?.toString()
        0 == beanRespuesta.ArchitectureHeader.ErrorHeader.ErrorMessage.size()
        0 == beanRespuesta.ArchitectureHeader.ReprocessingHeader.RetriesNumber.size()
        0 == beanRespuesta.ArchitectureHeader.ReprocessingHeader.OriginalInstanceId.size()
        "suser" == beanRespuesta?.ArchitectureHeader?.SecurityHeader?.User?.toString()
        "spass" == beanRespuesta?.ArchitectureHeader?.SecurityHeader?.Password?.toString()

        auditStop != null;
        validator.validate(auditStop?.text).errors == []
        def beanAuditStop = new XmlSlurper().parseText(auditStop?.text)
        '11111111222233334444555555555555' == beanAuditStop.CLEHeader.E2EId.text()
        '0'   == beanAuditStop.CLEHeader.E2EDeepness.text()
        'BBVAUser' == beanAuditStop.CLEHeader.InvocationUser.text()
        '567' == beanAuditStop.CLEHeader.BatchId.text()
        'AUD-ARC-0002' == beanAuditStop.CLEHeader.EventCode.text() //initial
        '1' == beanAuditStop.CLEHeader.EventType.text() //audit
        1 == beanAuditStop.CLEHeader.Timestamp.size()
        'S_KYGN_CA_SEND_PLAN' == beanAuditStop.CLEHeader.ServiceId.text()
        //   testUUIDbared == beanAuditStop.CLEHeader.InstanceId.text()
        'TIBCO_BE_CLE_TESTSAMPLE' == beanAuditStop.CLEHeader.ProjectName.text()
        'clebetesto' == beanAuditStop.CLEHeader.ProcessName.text()
        1 == beanAuditStop.CLEHeader.EngineName.size()
        1 == beanAuditStop.CLEHeader.MachineName.size()
        1 == beanAuditStop.Body.size()
        beanAuditStop.Body.text().contains(functionalPayloadMark)

        where:
        tracelevel | QE2ECtrl || algo
        //0          | ""       || 0//NO_INFO=0;
        //1          | ""       || 1//CRITICAL=1;
        //2          | ""       || 2//INFO=2;
        //4          | "true"   || 3//DEBUG=3;
        5          | "false"  || 4//TRACE=4;
    }

    @Timeout (value=6000)
    def "prueba de finalizacion KO del servicio"(){
        given:
        def initMessage = createBasicInput("""
                  <ns1:FunctionalPayload xmlns:ns1="http://tests.soap/be/audit/functionalPayload">
                     <ns1:A>A data</ns1:A>
                     <ns1:B>B data</ns1:B>
                     <ns1:scenarioParam key="scenario" value="ko">param1</ns1:scenarioParam>
                     <ns1:scenarioParam key="param" value="kocode">ERR-ARC-0002</ns1:scenarioParam>
                     <ns1:scenarioParam key="param" value="kodesc">error simulado por pruebas</ns1:scenarioParam>
                     <ns1:scenarioParam key="param" value="konatcode">NativeErrorCode</ns1:scenarioParam>
                     <ns1:scenarioParam key="param" value="konatdesc">NativeErrorDescription</ns1:scenarioParam>
                     <ns1:scenarioParam key="param" value="stack">nullPointer</ns1:scenarioParam>
                     <ns1:scenarioParam key="param" value="stackp">a,b,c,d</ns1:scenarioParam>
                     <ns1:scenarioParam key="param" value="severity">3</ns1:scenarioParam>
                     <ns1:scenarioParam key="param" value="FunctionalErrorData">FunctionalErrorData</ns1:scenarioParam>
                     <ns1:scenarioParam key="param" value="Category">ARC</ns1:scenarioParam>
                  </ns1:FunctionalPayload>""")

        //""" " """;

        when: //peticion basica
        jms.writeQueue(projectInput,initMessage)
        Message auditStart = jms.readQueue(auditoria, jmsTimeout)
        Message auditStop = jms.readQueue(auditoria, jmsTimeout)

        then:
        auditStart != null;
        auditStop != null;
        validator.validate(auditStop?.text).errors == []
        def beanAuditStop = new XmlSlurper().parseText(auditStop?.text)
        '11111111222233334444555555555555' == beanAuditStop.CLEHeader.E2EId.text()
        '0'   == beanAuditStop.CLEHeader.E2EDeepness.text()
        'BBVAUser' == beanAuditStop.CLEHeader.InvocationUser.text()
        '567' == beanAuditStop.CLEHeader.BatchId.text()
        1 == beanAuditStop.CLEHeader.Timestamp.size()
        'S_KYGN_CA_SEND_PLAN' == beanAuditStop.CLEHeader.ServiceId.text()
        'TIBCO_BE_CLE_TESTSAMPLE' == beanAuditStop.CLEHeader.ProjectName.text()
        'clebetest' == beanAuditStop.CLEHeader.ProcessName.text()
        1 == beanAuditStop.CLEHeader.EngineName.size()
        1 == beanAuditStop.CLEHeader.MachineName.size()
        'ERR-ARC-0002' == beanAuditStop.CLEHeader.EventCode.text()
        '3' == beanAuditStop.CLEHeader.EventType.text() //error
        'ERR-ARC-0002' == beanAuditStop.CLEHeader.ErrorHeader.ErrorCode.text()
        'error simulado por pruebas' == beanAuditStop.CLEHeader.ErrorHeader.ErrorDescription.text()
        'NativeErrorCode' == beanAuditStop.CLEHeader.ErrorHeader.NativeErrorCode.text()
        'NativeErrorDescription' == beanAuditStop.CLEHeader.ErrorHeader.NativeErrorDescription.text()
        'nullPointer' == beanAuditStop.CLEHeader.ErrorHeader.StackTrace.text()
        1 == beanAuditStop.CLEHeader.ErrorHeader.ErrorDate.size()
        'a,b,c,d' == beanAuditStop.CLEHeader.ErrorHeader.StackProcess.text()
        '3' == beanAuditStop.CLEHeader.ErrorHeader.Severity.text()
        'ARC' == beanAuditStop.CLEHeader.ErrorHeader.Category.text()
        1 == beanAuditStop.CLEHeader.ErrorHeader.ProcessInputMessage.size()
        '0' == beanAuditStop.CLEHeader.ErrorHeader.IsResponse.text()
        'CDM' == beanAuditStop.CLEHeader.ErrorHeader.MessageType.text()
        "FunctionalErrorData" == beanAuditStop.CLEHeader.ErrorHeader.FunctionalErrorData.text()
        "1212-12-12T12:12:12" == beanAuditStop.CLEHeader.ErrorHeader.ErrorDate.text()
        new XmlSlurper().parseText(auditStart.text).CLEHeader.InstanceId.text() == beanAuditStop.CLEHeader.InstanceId.text()

        beanAuditStop?.Body  != null
    }

    @Timeout (value=6000)
    def "prueba de ejecucion escenario sin respuesta"(){
        given:
        def initMessage = createBasicInput("""
                  <ns1:FunctionalPayload xmlns:ns1="http://tests.soap/be/audit/functionalPayload">
                     <ns1:A>A data</ns1:A>
                     <ns1:B>B data</ns1:B>
                     <ns1:scenarioParam key="scenario" value="baseSinRespuesa">param1</ns1:scenarioParam>
                  </ns1:FunctionalPayload>""")

        when: //peticion basica
        jms.writeQueue(projectInput,initMessage)
        Message auditStart = jms.readQueue(auditoria, jmsTimeout)
        Message auditStop = jms.readQueue(auditoria, jmsTimeout)
        Message response = jms.readQueue(projectOutput, 0)


        then:
        response == null

        'AUD-ARC-0001'== (new XmlSlurper().parseText(auditStart?.text)).CLEHeader.EventCode.text()

        auditStop != null;
        validator.validate(auditStop?.text).errors == []
        def beanAuditStop = new XmlSlurper().parseText(auditStop?.text)
        '11111111222233334444555555555555' == beanAuditStop.CLEHeader.E2EId.text()
        '0'   == beanAuditStop.CLEHeader.E2EDeepness.text()
        'BBVAUser' == beanAuditStop.CLEHeader.InvocationUser.text()
        '567' == beanAuditStop.CLEHeader.BatchId.text()
        'AUD-ARC-0002' == beanAuditStop.CLEHeader.EventCode.text() //initial
        '1' == beanAuditStop.CLEHeader.EventType.text() //audit
        1 == beanAuditStop.CLEHeader.Timestamp.size()
        'S_KYGN_CA_SEND_PLAN' == beanAuditStop.CLEHeader.ServiceId.text()
        // testUUIDbared == beanAuditStop.CLEHeader.InstanceId.text()
        'TIBCO_BE_CLE_TESTSAMPLE' == beanAuditStop.CLEHeader.ProjectName.text()
        'Rules.scenarios.BaseNoResponse' == beanAuditStop.CLEHeader.ProcessName.text()
        1 == beanAuditStop.CLEHeader.EngineName.size()
        1 == beanAuditStop.CLEHeader.MachineName.size()
        beanAuditStop.Body.children().isEmpty()
    }

    /** al probar con error tambien se prueba
     * - que se maneja correctamente la respuesta
     * - que funciona la auditoria con enventos en la caché
     */
    @Timeout (value=6000)
    def "prueba de notificacion con error y de respuesta con dos rtc -cache-"() {
        given:
        def initMessage = createBasicInput("""
                  <ns1:FunctionalPayload xmlns:ns1="http://tests.soap/be/audit/functionalPayload">
                     <ns1:A>A data</ns1:A>
                     <ns1:B>B data</ns1:B>
                     <ns1:scenarioParam key="scenario" value="notif">param1</ns1:scenarioParam>
                  </ns1:FunctionalPayload>""")
        def notificationFakeResponse = CreateNotificationResponse('0000','Operacion realizada con éxito')
        def notificationResponse = CreateNotificationResponse(notifResponseCode,'Tipo de tonificación no activa')

        when: //peticion basica
        jms.writeQueue(projectInput, initMessage)
        Message auditStart = jms.readQueue(auditoria, jmsTimeout)
        Message notifRq = jms.readQueue(notificacion, jmsTimeout)
        def correlation = notifRq?.getJMSCorrelationID()
        jms.writeQueue(notificacionRsp,notificationFakeResponse,'fakeCorrelation')
        jms.writeQueue(notificacionRsp,notificationResponse,correlation)
        Message auditStop = jms.readQueue(auditoria, jmsTimeout)
        Message response = jms.readQueue(projectOutput, 1) //intercambiado por el anterior para poder quitar el jmsTimeout


        then:
        //notificacion
        correlation != null
        byte[] byteNotifRq = new byte[notifRq.bodyLength]
        notifRq.readBytes(byteNotifRq)
        String strNotifRq = new String(byteNotifRq)
        def beanNotifRq = new XmlSlurper().parseText(strNotifRq) //byteMessage
        ! beanNotifRq.Body.sendNotificationRequest.TipoNotificacion.isEmpty()
        //auditoria
        auditStop != null;
        validator.validate(auditStop?.text).errors == []
        def beanAuditStop = new XmlSlurper().parseText(auditStop?.text)
        System.println auditStop.text
        '11111111222233334444555555555555' == beanAuditStop.CLEHeader.E2EId.text()
        errorCode == beanAuditStop.CLEHeader.EventCode.text()
        //mensaje
        messageSent == ( null != response);
        if(response!=null){
            validator.validate(response?.text).errors == []
            def beanRespuesta = new XmlSlurper().parseText(response?.text)
            "S_KYGN_CA_SEND_PLAN" == beanRespuesta.ArchitectureHeader.AuditHeader.ServiceId.text //header tiene datos de la cache
            ! beanRespuesta.Body.sendNotificationResponse.ErrorCode.isEmpty() //el evento es el esperado
        }

        where:
        notifResponseCode ||errorCode       |messageSent
        'ERR-FUN-0301'    ||'ERR-ARC-0002'  |false
        '0000'            ||'AUD-ARC-0002'  |true
    }

    @Timeout (value=3000)
    def "prueba de envio de epub base"(){
        given:
        System.println "++ <ns0:InstanceId>${testUUIDbared}</ns0:InstanceId>"
        def initMessage = createBasicInput("""
                      <ns1:FunctionalPayload xmlns:ns1="http://tests.soap/be/audit/functionalPayload">
                         <ns1:A>A data</ns1:A>
                         <ns1:B>B data</ns1:B>
                         <ns1:scenarioParam key="scenario" value="epubBase"></ns1:scenarioParam>
                         <ns1:scenarioParam key="param" value="EType">${eventType}</ns1:scenarioParam>
                         <ns1:scenarioParam key="param" value="isService">${isService}</ns1:scenarioParam>
                         <ns1:scenarioParam key="param" value="optionals">${optionals}</ns1:scenarioParam>
                      </ns1:FunctionalPayload>
                """);

        when: //peticion basica
        jms.writeQueue(projectInput,initMessage)
        Message auditStart = jms.readQueue(auditoria, jmsTimeout)
        Message pub = jms.readQueue(epub,jmsTimeout)
        Message auditTrace = jms.readQueue(auditoria, jmsTimeout)
        Message auditStop = jms.readQueue(auditoria, jmsTimeout)

        then: //epub
        pub != null
        validator.validate(pub?.text).errors == []
        def beanPub = new XmlSlurper().parseText(pub?.text)
        if (isService){//está encapsulado en un message
            !beanPub.Header.empty
            beanPub = beanPub.Body.Event
        }
        ;              "pEventName" == beanPub.EventName.text()
        ;                "pVersion" == beanPub.Version.text()
        ;                    "KYGN" == beanPub.EventEmitter.text()
        ;             eventTypeCode == Integer.parseInt(beanPub.EventType.text())
        ;                            ! beanPub.Machine.isEmpty()
        ;                            ! beanPub.Timestamp.isEmpty()
        ;                            ! beanPub.EventUUID.isEmpty()
 '11111111222233334444555555555555' == beanPub.FrameworkData.ExecutionData.E2EId.text()
        ;                       '0' == beanPub.FrameworkData.ExecutionData.E2EDeepness.text()
        ;                'BBVAUser' == beanPub.FrameworkData.ExecutionData.InvocationUser.text()
        ;                     '567' == beanPub.FrameworkData.ExecutionData.BatchId.text()
        ;                        32 == beanPub.FrameworkData.ExecutionData.InstanceId.text().size()
 '23141111222233334444555555555555' == beanPub.FrameworkData.ExecutionData.ParentInstanceId.text()
        ;                     '456' == beanPub.FrameworkData.ExecutionData.ExternalId.text()
        ;'2014-01-28T01:37:00.155Z' == beanPub.FrameworkData.ExecutionData.StarTimestamp.text()
        ;     'S_KYGN_CA_SEND_PLAN' == beanPub.FrameworkData.ExecutionData.ServiceIdStater.text()
        ;                              beanPub.FrameworkData.ExecutionData.ServiceDestinations.isEmpty()
        ;                    'CHAA' == beanPub.FrameworkData.ServiceData.ServiceType.text()
        ;     'S_KYGN_CA_SEND_PLAN' == beanPub.FrameworkData.ServiceData.ServiceId.text()
        optionals||'BusinessObject' == beanPub.FrameworkData.ServiceData.BusinessObject.text()
        optionals|| 'pnServiceName' == beanPub.FrameworkData.ServiceData.ServiceName.text()
        optionals||   'pnSourceApp' == beanPub.FrameworkData.ServiceData.SourceApp.text()
        optionals||   'pnTargetApp' == beanPub.FrameworkData.ServiceData.TargetApp.text()
        optionals||           '100' == beanPub.FrameworkData.ServiceData.Domain.text()
                         'A data22' == beanPub.Body.AltPayload.text()

        !optionals||   beanPub.FrameworkData.ServiceData.SourceApp.isEmpty()
        !optionals||   beanPub.FrameworkData.ServiceData.TargetApp.isEmpty()
        !optionals||   beanPub.FrameworkData.ServiceData.Domain.isEmpty()
        !optionals||   beanPub.FrameworkData.ServiceData.ServiceName.isEmpty()


        and: //auditoria
        //auditoria start
        assertSimpleAuditMessage(auditStart,'AUD-ARC-0001','S_KYGN_CA_SEND_PLAN',testUUIDbared,'clebetest')//initial
        //auditoria trace --> detalle completo
        auditTrace != null;
        validator.validate(auditTrace?.text).errors == []
        def beanAuditTrace = new XmlSlurper().parseText(auditTrace?.text)
 '11111111222233334444555555555555' == beanAuditTrace.CLEHeader.E2EId.text()
        ;                     '0'   == beanAuditTrace.CLEHeader.E2EDeepness.text()
        ;                'BBVAUser' == beanAuditTrace.CLEHeader.InvocationUser.text()
        ;                     '567' == beanAuditTrace.CLEHeader.BatchId.text()
        ;            'TRC-ARC-0001' == beanAuditTrace.CLEHeader.EventCode.text() //initial
        ;                       '2' == beanAuditTrace.CLEHeader.EventType.text() //audit
        ;                         1 == beanAuditTrace.CLEHeader.Timestamp.size()
        ;     'S_KYGN_CA_SEND_PLAN' == beanAuditTrace.CLEHeader.ServiceId.text()
        ;                        32 == beanAuditTrace.CLEHeader.InstanceId.text()?.size()
        ; 'TIBCO_BE_CLE_TESTSAMPLE' == beanAuditTrace.CLEHeader.ProjectName.text()
        ;'Rules.scenarios.EPubBase' == beanAuditTrace.CLEHeader.ProcessName.text()
        ;                         1 == beanAuditTrace.CLEHeader.EngineName.size()
        ;                         1 == beanAuditTrace.CLEHeader.MachineName.size()
        ;     beanAuditTrace?.Body  != null
        ;                         1 == beanAuditTrace.Body.size()
        //auditoria stop
        assertSimpleAuditMessage(auditStop,'AUD-ARC-0002','S_KYGN_CA_SEND_PLAN',testUUIDbared,'Process no response') //final


        where:
        isService | eventType   | optionals || eventTypeCode
        true      | 'Info'      | true      || 1
        false     | 'Error'     | true      || 2
        false     | 'Heartbeat' | true      || 4
        false     | 'Alarm'     | false     || 3
        false     | ''          | true      || 0
    }
}
