package bpmTestTronesTest

import groovy.util.slurpersupport.GPathResult
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Timeout
import tools.jms.EmsSourceImp
import tools.jms.JmsInterface
import tools.xml.XsdValidator
import tools.bpm.AmxBpmTester
import javax.jms.Message

class TestTrones extends Specification {
    //variables compartidas por todas las pruebas
    @Shared jmsTimeout = 3 * 1000
    @Shared htTimeout = 60 * 1000
    @Shared JmsInterface jms = new EmsSourceImp("tcp://localhost:7222","admin","")
    @Shared XsdValidator validator
    @Shared AmxBpmTester bpmAdminUser;


    //variables inicializadas para cada prueba
    def testUUID =  java.util.UUID.randomUUID().toString()

    //inicializacion del conjunto de pruebas
    def setupSpec() {
        jms.timeout = jmsTimeout
        jms.connect()
        //validator = new XsdValidator(logOnError: true)
        //validator.addXSDFolder new File('../TIBCO_BE_CLE/Architecture/SharedResources')
        bpmAdminUser = new AmxBpmTester();//parametros de conexión por defecto
    }

    //construcción de cada prueba
    def setup (){
        bpmAdminUser.AbortAllProessInstances();
    }

    //destrucci�n dle conjunto de pruebas
    def cleanupSpec(){
        bpmAdminUser = null;
        jms.disconnect()
    }

    //////////////////////////////////////////////////////// datos /////////////////////////////////////////////


    /** verifica un evento sin tener en cuenta todos sus datos */
    private void assertSimpleAuditMessage(Message auditEvent, String eventCode,String serviceId, String instanceId, String procesName) {
        assert auditEvent != null;
        assert validator.validate(auditEvent?.text).valid
        GPathResult beanAudit = new XmlSlurper().parseText(auditEvent?.text)
        assert eventCode == beanAudit.CLEHeader.EventCode.text()
        assert serviceId == beanAudit.CLEHeader.ServiceId.text()
        assert procesName == beanAudit.CLEHeader.ProcessName.text()
        //    assert instanceId == beanAudit.CLEHeader.InstanceId.text()
    }

    //////////////////////////////////////////////////////// escenarios /////////////////////////////////////////////
    @Timeout (value=60000)
    def "prueba de ejecucion escenario elemental"(){
        given:
        def humanTaskResponseCreation = """<xml-fragment xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
            <outputs array="false" name="GerraRef" type="Data Reference">
                <simpleSpec/>
            </outputs>
            <inouts array="false" name="Gerra" type="Complex">
                <complexSpec>
                    <value>
                        <tronodata:GerraElement xsi:type="tronodata:Gerra" xmlns:tronodata="http://example.com/tronodata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                            <contendiente>el mal</contendiente>
                            <objetivo>vencer al mal ${testUUID}</objetivo>
                        </tronodata:GerraElement>
                    </value>
                </complexSpec>
            </inouts>
        </xml-fragment>""";
        def humanTaskResponseReason = """<xml-fragment xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
            <inputs array="false" name="GerraRef" type="Data Reference"><simpleSpec/></inputs>
            <inouts array="false" name="Gerra" type="Complex">
                <complexSpec>
                    <value>
                        <tronodata:GerraElement xsi:type="tronodata:Gerra" xmlns:tronodata="http://example.com/tronodata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                            <contendiente>el mal</contendiente>
                            <excusa>no necesitamos motivo</excusa>
                            <objetivo>vencer al mal ${testUUID}</objetivo>
                        </tronodata:GerraElement>
                    </value>
                </complexSpec>
            </inouts>
            </xml-fragment>""";
        def humanTaskResponseNotification = null;


        when:
        bpmAdminUser.startProcess "OrganizarGuerra"
        def htRequestCreate = bpmAdminUser.closeHumanTask("GerraCreateCaseData", htTimeout , humanTaskResponseCreation)
        def htrequestExcusa = bpmAdminUser.closeHumanTask("Inventarexcusa", htTimeout , humanTaskResponseReason)
        def htrequestNotif  = bpmAdminUser.closeHumanTask("Notificardeclaracindeguerra", htTimeout , humanTaskResponseNotification)

        then:
        notThrown(Exception) //no ha habido errores
        def excusaQuery = new XmlSlurper().parseText(htrequestExcusa)
        "vencer al mal ${testUUID}" == excusaQuery.inouts.complexSpec.value.GerraElement.objetivo.text()
        0 == excusaQuery.inouts.complexSpec.value.GerraElement.excusa.size()

        def notifQuery = new XmlSlurper().parseText(htrequestNotif)
        "vencer al mal ${testUUID}" == notifQuery.inouts.complexSpec.value.GerraElement.objetivo.text()
        'no necesitamos motivo' == notifQuery.inouts.complexSpec.value.GerraElement.excusa.text()
    }

    @Timeout (value=60000)
    def "prueba de ejecucion sin actividad de excusa"(){
        given:
        def humanTaskResponseCreation = """<xml-fragment xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
            <outputs array="false" name="GerraRef" type="Data Reference">
                <simpleSpec/>
            </outputs>
            <inouts array="false" name="Gerra" type="Complex">
                <complexSpec>
                    <value>
                        <tronodata:GerraElement xsi:type="tronodata:Gerra" xmlns:tronodata="http://example.com/tronodata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                            <contendiente>Donald</contendiente>
                            <excusa>di no a los patos sin pantalones</excusa>
                            <objetivo>vencer ${testUUID}</objetivo>
                        </tronodata:GerraElement>
                    </value>
                </complexSpec>
            </inouts>
        </xml-fragment>""";
        def humanTaskResponseNotification = null;


        when:
        bpmAdminUser.startProcess "OrganizarGuerra"
        def htRequestCreate = bpmAdminUser.closeHumanTask("GerraCreateCaseData", htTimeout , humanTaskResponseCreation)
        //def htrequestExcusa = bpmAdminUser.closeHumanTask("Inventarexcusa", htTimeout / 3, null)
        def htrequestNotif  = bpmAdminUser.closeHumanTask("Notificardeclaracindeguerra", htTimeout , humanTaskResponseNotification)

        then:
        notThrown(Exception) //no ha habido errores

        //htrequestExcusa == null //descomentando esta linea y la asociada, se puede comprobar explicitamente que no existe una determinada tarea manual

        def notifQuery = new XmlSlurper().parseText(htrequestNotif)
        "vencer ${testUUID}" == notifQuery.inouts.complexSpec.value.GerraElement.objetivo.text()
        'di no a los patos sin pantalones' == notifQuery.inouts.complexSpec.value.GerraElement.excusa.text()
    }

}
