package tools.jms

import javax.jms.DeliveryMode
import javax.jms.ExceptionListener
import javax.jms.JMSException
import javax.jms.Message
import javax.jms.MessageConsumer
import javax.jms.MessageProducer
import javax.jms.Session

/**
 * Created by Administrator on 7/8/2014.
 */
public interface JmsInterface {
    def timeout

    def connect ()

    def disconnect ()

    javax.jms.Queue createQueue(String name)

    def  Message readQueue(javax.jms.Queue queue, Integer timeout1)

    def  void writeQueue(javax.jms.Queue queue, String message, String correlationId)

    def  void writeQueue(javax.jms.Queue queue, String message)

    def  void purgeQueue(ArrayList<javax.jms.Queue> colas)
}