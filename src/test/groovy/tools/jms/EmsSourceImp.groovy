package tools.jms

import com.tibco.tibjms.TibjmsConnectionFactory

import javax.jms.Connection
import javax.jms.ConnectionFactory
import javax.jms.DeliveryMode
import javax.jms.Destination
import javax.jms.ExceptionListener
import javax.jms.JMSException
import javax.jms.Message
import javax.jms.MessageConsumer
import javax.jms.MessageProducer
import javax.jms.Session

/**
 * Created by Administrator on 7/8/2014.
 */
class EmsSourceImp implements JmsInterface {
    ConnectionFactory jmsFactory
    Connection conexionJMS
    Session sesionJMS
    def timeout = 3000

    public EmsSourceImp(final String connectionUrl, final String connectionUserName, final String connectionPassword){
        jmsFactory = new TibjmsConnectionFactory(connectionUrl){{setUserName(connectionUserName);setUserPassword(connectionPassword);}};
    }

    def connect () {
        //conexion al jms
        conexionJMS = jmsFactory.createConnection();
        //activamos la conexion para lectura
        conexionJMS.clientID = "spock:${System.currentTimeMillis()}";
        conexionJMS.exceptionListener = {JMSException e -> logger.error("JMS Exception", e)} as ExceptionListener;
        //creamos la sesión para escrituras
        sesionJMS = conexionJMS.createSession(false, Session.AUTO_ACKNOWLEDGE);
        conexionJMS.start()
    }

    def disconnect () {
        sesionJMS.close()
        conexionJMS.stop()
        conexionJMS.close()
    }

    javax.jms.Queue createQueue(String name){
        sesionJMS.createQueue(name)
    }

    //////////////////////////////////////////////////////// JMS /////////////////////////////////////////////
    //funcion auxiliar que lee un mensaje de una cola
    def  Message readQueue(javax.jms.Queue queue, Integer timeout1){
        def slices = 20 //numero de reintentos durante el periodo de jmsTimeout
        long count
        long time = (long)(timeout1/slices)
        Message result = null
        MessageConsumer auditConsumer = sesionJMS.createConsumer(queue)
        for (count=0 ; count<slices && result == null ; count++){
            result = auditConsumer.receiveNoWait()//no funciona bien el metodo de lectura bloqueante
            sleep time
        }
        auditConsumer?.close();
        return result
    }


    void writeQueue(javax.jms.Queue queue, String message){
        writeQueue(queue,message,null)
    }

    //funcion auxiliar para escribir un mensaje en una cola
    def  void writeQueue(javax.jms.Queue queue, String message, String correlationId){
        MessageProducer producer
        producer = sesionJMS.createProducer(queue)
        producer.deliveryMode = DeliveryMode.NON_PERSISTENT
        producer.timeToLive = timeout

        def textMessage = sesionJMS.createTextMessage(message)
        if(correlationId != null){
            textMessage.setJMSCorrelationID(correlationId);
        }
        producer.send(textMessage)
        producer.close()
    }

    //funcion auxiliar para purgar colas
    def void purgeQueue(ArrayList<javax.jms.Queue> colas) {
        def purged = []
        colas.each() {
            Message chunk
            for(chunk = readQueue(it,1);chunk = readQueue(it,1);it!=null){
                purged.add(it)
            }
        }
    }
}
