package tools.bpm;

import com.tibco.n2.brm.api.GetWorkListItemsResponseDocument;
import com.tibco.n2.brm.api.WorkItem;
import com.tibco.n2.brm.services.*;
import com.tibco.n2.common.datamodel.DataModel;
import com.tibco.n2.de.services.InternalServiceFault;
import com.tibco.n2.de.services.InvalidServiceRequestFault;
import com.tibco.n2.de.services.SecurityFault;
import com.tibco.n2.process.management.api.CreateProcessInstanceInputDocument.CreateProcessInstanceInput.ParameterMap;
import com.tibco.n2.process.management.services.IllegalArgumentFault;
import com.tibco.n2.process.management.services.OperationFailedFault;
import com.tibco.n2.service.adapter.config.Configuration;
import org.apache.xmlbeans.XmlException;

import java.util.Date;

/**
 * Created by user on 10/28/15.
 */
public class AmxBpmTester {
    AmxBpmClientController amxBpmClientController;
    public static final String DEFAULT_PASSWORD="tibco123"; //password de los usuarios del easyldap
    public static final Configuration.Protocol HTTP = Configuration.Protocol.HTTP;
    public static final Configuration.Protocol HTTPS = Configuration.Protocol.HTTPS;

    //constructores
    public AmxBpmTester (Configuration.Protocol protocol, String host, int port, String userName, String userPassword) throws InternalServiceFault, InvalidServiceRequestFault, SecurityFault {
        this.amxBpmClientController = new AmxBpmClientController(protocol, host, port, userName, userPassword);
    }

    public AmxBpmTester (String userName, String userPassword) throws InternalServiceFault, InvalidServiceRequestFault, SecurityFault {
        this(Configuration.Protocol.HTTP, "127.0.0.1", 8080, userName, userPassword);
    }

    public AmxBpmTester () throws InternalServiceFault, InvalidServiceRequestFault, SecurityFault {
        this(Configuration.Protocol.HTTP, "127.0.0.1", 8080, "tibco-admin", "secret");
    }

    //test metods

    /** pide al administrator que aborte todas las instancias de procesos.
     *  util para limpiar el entorno entre pruebas.
     * @throws OperationFailedFault
     * @throws IllegalArgumentFault
     */
    public void AbortAllProessInstances () throws OperationFailedFault, IllegalArgumentFault {
        amxBpmClientController.purgeProcessInstances();
    }

    /** Procedimiento simple para iniciar procesos.
     * no acepta ni nombre ni version ni especificar que actividad de inicio utilizar
     * @param nombre del proceso
     * @return processID
     */
    public String startProcess (String nombre) throws IllegalArgumentFault, AmxBpmClientController.ProcessNotFound, OperationFailedFault {
        return startProcess (nombre, null);
    }

    /** Procedimiento simple para iniciar procesos.
     * no acepta ni nombre ni version ni especificar que actividad de inicio utilizar
     * @param nombre del proceso
     * @param parametros parametros de entrada (para subrpocesos)
     * @return processID
     */
    public String startProcess (String nombre, ParameterMap parametros) throws IllegalArgumentFault, AmxBpmClientController.ProcessNotFound, OperationFailedFault {
        return amxBpmClientController.CreateProcessInstance(nombre, parametros);
    }

    /** espera hasta que exista una tarea manual con un nombre dado
     *
     * @param name nombre de la tarea manual, si null, espera a cualquier tarea manual
     * @param timeoutMilisecs tiempo (milisecs) que máximo de espera, CreateProcessInstance tarda alrededor de 15 segundos
     *                        en crear la primera tarea manual.
     * @return tarea manual.
     */
    public WorkItem waitForHumanTask(String name, long timeoutMilisecs) throws WorkItemOrderFault, com.tibco.n2.brm.services.InternalServiceFault, InvalidEntityFault, WorkItemFilterFault, com.tibco.n2.brm.services.SecurityFault {
        GetWorkListItemsResponseDocument.GetWorkListItemsResponse taskList;
        long timeout = (new Date()).getTime()+timeoutMilisecs;
        long ahora;
        do {
            taskList = amxBpmClientController.getWorkitems(name);
            ahora = (new Date()).getTime();
        } while (taskList.getTotalItems() == 0 && ahora<timeout );
        if (taskList.getTotalItems() == 0){
            return null;
        } else {
            return taskList.getWorkItemsArray(0);
        }
    }

    /**
     * Cierra una tarea manual conocida.
     * @param HUmanTask tarea manual, puede ser obtenida de waitForHumanTask()
     * @param xmlData datos de respuesta, puede ser null si se desea traspasar la entrada a la salida sin cambios, esto
     *                suele ser util para tareas de usuario sin inteterface.
     * @return xml con los datos que se envian al formulario para presentarselos al usuario
     */
    public String closeHumanTask (WorkItem HUmanTask, String  xmlData) throws OpenWorkItemLimitFault, WorkItemFault, InvalidWorkItemFault, InvalidVersionFault, com.tibco.n2.brm.services.InternalServiceFault, WorkItemAPIScriptCancelFault, com.tibco.n2.brm.services.SecurityFault, XmlException, InvalidEntityFault {
        DataModel response;
        response = amxBpmClientController.completeWorkItem(HUmanTask, xmlData);
        return response.xmlText();
    }

    //TODO sin hacer
    public String closeHumanTask (String name, long timeoutMilisecs, String xmlData) throws com.tibco.n2.brm.services.InternalServiceFault, WorkItemOrderFault, WorkItemFilterFault, InvalidEntityFault, com.tibco.n2.brm.services.SecurityFault, InvalidVersionFault, OpenWorkItemLimitFault, WorkItemFault, WorkItemAPIScriptCancelFault, InvalidWorkItemFault, XmlException {
        WorkItem HT = waitForHumanTask(name, timeoutMilisecs);
        if (HT == null){
            return null;
        }
        String response = closeHumanTask(HT,xmlData);
        return response;
    }

}
