package tools.bpm;
/**
 * Created by user on 10/27/15.
 */
import com.tibco.bpm.service.connector.ServiceConnector;
import com.tibco.bpm.service.connector.ServiceConnectorFactory;
import com.tibco.n2.brm.api.*;
import com.tibco.n2.brm.services.*;
import com.tibco.n2.common.datamodel.DataModel;
import com.tibco.n2.common.organisation.api.OrganisationalEntityType;
import com.tibco.n2.de.api.resolver.LookupUserResponseDocument;
import com.tibco.n2.de.services.InternalServiceFault;
import com.tibco.n2.de.services.InvalidServiceRequestFault;
import com.tibco.n2.de.services.SecurityFault;
import com.tibco.n2.process.management.api.*;
import com.tibco.n2.process.management.services.IllegalArgumentFault;
import com.tibco.n2.process.management.services.IllegalStateFault;
import com.tibco.n2.process.management.services.OperationFailedFault;
import com.tibco.n2.service.adapter.config.Configuration;
import com.tibco.n2.service.connector.config.context.DefaultSecurityHandler;
import com.tibco.n2.service.connector.config.context.SecurityHandler;
import org.apache.xmlbeans.XmlException;

import java.util.logging.Logger;


public class AmxBpmClientController {

    ServiceConnector serviceConnector;//connector a todos los servicios
    String guid;
    String userName;

    public AmxBpmClientController(Configuration.Protocol protocol, String host, int port, String userName, String userPassword) throws InternalServiceFault, InvalidServiceRequestFault, SecurityFault {
        serviceConnector = init (protocol, host, port, userName, userPassword);
        LookupUserResponseDocument.LookupUserResponse userResponse = serviceConnector.getEntityResolverService().lookupUser(userName, null, null, true);
        guid = userResponse.getDetailArray(0).getGuid();
        this.userName = userName;

    }

    private ServiceConnector init( Configuration.Protocol protocol, String host, int port, String userName, String userPassword){
        System.setProperty("amxbpm.soap.specification","SOAP_1_2");
        SecurityHandler securityHandler = new DefaultSecurityHandler(userName, userPassword);
        ServiceConnector serviceConnector = ServiceConnectorFactory.getServiceConnector (
                protocol,
                host,
                port,
                securityHandler);
        return serviceConnector;
    }

    public GetWorkListItemsResponseDocument.GetWorkListItemsResponse getWorkitems(String humanTaskName) throws com.tibco.n2.brm.services.SecurityFault, com.tibco.n2.brm.services.InternalServiceFault, InvalidEntityFault, WorkItemFilterFault, WorkItemOrderFault {
        int startPos = 0;
        int numberOfItems = 10;
        com.tibco.n2.de.api.XmlModelEntity entityId = this.getEntityId();
        OrderFilterCriteria orderFilterCriteria;
        if (humanTaskName == null) {
            orderFilterCriteria = null;
        } else {
            orderFilterCriteria = OrderFilterCriteria.Factory.newInstance();
            orderFilterCriteria.setFilter("name = '"+humanTaskName+"'");
        }
        GetWorkListItemsResponseDocument.GetWorkListItemsResponse response;

        response = serviceConnector.getWorkListService().getWorkListItems(
                orderFilterCriteria,
                entityId,
                startPos,
                numberOfItems);

        return response;
    }

    /** objetivo: borrar todas las instancias de proceso para hacer pruebas más limpias */
    public void purgeProcessInstances() throws OperationFailedFault, IllegalArgumentFault {
        QualifiedProcessName processName = QualifiedProcessName.Factory.newInstance(); //sin parametros equivale a "todos"
        ProcessInstance[] processInstances = serviceConnector.getProcessManagerService().listProcessInstances(processName);
        for (ProcessInstance processInstance : processInstances){
            try {
                serviceConnector.getProcessManagerService().cancelProcessInstance(processInstance.getId());
            } catch (IllegalStateFault illegalStateFault) {
                Logger.getLogger("purgeProcessInstance").warning("unable to purge "+processInstance.getId());
            }
        }
    }

    /** objetivo: crear una instancia de proceso.
     *  ver pagina 213 del manual
     *  @param processName opcional, nombre del proceso, si se pasa null se inicia el primer template que se encuentre.
     *  @param parameterMap opcional, parametros del proceso, null si no tiene.
     *  @return process id
     */
    public String CreateProcessInstance(String processName, CreateProcessInstanceInputDocument.CreateProcessInstanceInput.ParameterMap parameterMap) throws OperationFailedFault, IllegalArgumentFault, ProcessNotFound {
        com.tibco.bpm.service.connector.ProcessManagerService processManagerService = serviceConnector.getProcessManagerService();

        //get the template
        QualifiedProcessName qualifiedProcessName = QualifiedProcessName.Factory.newInstance();
        if ( processName != null )
            qualifiedProcessName.setProcessName(processName);
        BasicProcessTemplate[] templates = processManagerService.listProcessTemplates(qualifiedProcessName);
        if (templates == null || templates.length == 0)
            throw new ProcessNotFound(processName);
        BasicProcessTemplate template = templates [0];

        //get first starter operation
        StarterOperation starterOperation = processManagerService.listStarterOperations(template.getProcessQName())[0];

        //check operation info
        OperationInfo operationInfo = processManagerService.getStarterOperationInfo(template.getProcessQName(), starterOperation.getOperation());
        String processId = processManagerService.createProcessInstance(template.getProcessQName(),starterOperation.getOperation(), parameterMap);
        return processId;
    }

    /** cierra una tarea manual con unos datos.
     * la tarea manual se puede obtener de getWorkItems(), que incluye una lista de worktitems.
     * Si la respuesta es null se coloca la entrada de la tarea a la salida, esto es util para las tareas manuales que
     * (todavia) no tienen interface y no queremos modificar los datos.
     *
     * @param workItem tarea manual a cerrar
     * @param xmlResponse xml de respuesta, ver documentación dle formato. puede ser null
     * @return devuelve los datos de la tarea manual para poder verificarlos en una tarea manual.
     */
    public DataModel completeWorkItem(WorkItem workItem, String xmlResponse) throws OpenWorkItemLimitFault, WorkItemFault, InvalidWorkItemFault, com.tibco.n2.brm.services.SecurityFault, InvalidVersionFault, WorkItemAPIScriptCancelFault, com.tibco.n2.brm.services.InternalServiceFault, XmlException, InvalidEntityFault {
        ManagedObjectID [] managedObjectIDs = new ManagedObjectID[1];
        managedObjectIDs [0] = workItem.getId();
        String [] resources = new String[ 1 ];
        resources [0] = this.guid;
        WorkItem[] openedWorkItem ;
        CompleteWorkItemResponseDocument.CompleteWorkItemResponse closeResponse;
        DataModel taskInputData;
        DataModel taskResponseData;


        openedWorkItem = serviceConnector.getWorkItemManagementService().allocateAndOpenWorkItem(managedObjectIDs,resources);
        taskInputData = openedWorkItem[0].getBody().getDataModel();
        if (xmlResponse != null){
            taskResponseData = DataModel.Factory.parse(xmlResponse);
        } else {
            //if there is no response info the request is sent back unmodified
            taskResponseData = taskInputData;
        }
        WorkItemBody workItemPayload;
        workItemPayload = WorkItemBody.Factory.newInstance();
        workItemPayload.setDataModel(taskResponseData);
        closeResponse = serviceConnector.getWorkItemManagementService().completeWorkItem(
                openedWorkItem[0].getId(),
                workItemPayload,
                false);

        return taskInputData;

    }

    /** excepción lanzada por CreateProcessInstance cuando se especifica una plantilla que no existe */
    public class ProcessNotFound extends Exception{
        public ProcessNotFound (String msg){
            super( "Process "+msg+" not found!");
        }
    }


    private com.tibco.n2.de.api.XmlModelEntity getEntityId (){
        com.tibco.n2.de.api.XmlModelEntity result = com.tibco.n2.de.api.XmlModelEntity.Factory.newInstance();
        result.setGuid(this.guid);
        result.setName(this.userName);
        result.setEntityType(OrganisationalEntityType.RESOURCE);
        result.setModelVersion(-1);
        return result;
    }
}
