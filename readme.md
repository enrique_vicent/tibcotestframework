Descripción
============

Framework para facilitar las pruebas automáticas de productos basados en Tibco. 

Este framework facilita las pruebas de aquellos productos que se comunican con el mundo mediante tareas manuales ( Amx 
BPM) y/o colas EMS.

Se pueden encontrar ejemplos de pruebas para Tibco Business Events y Tibco AMX BPM


Componentes
---------
Se utiliza gradle y spock

- Gradle permite introducir librerias propietarias, pero la razón principal por la que se introdujo es porque se puede 
extender su ciclo de vida para incluir en el automatismo las fases de compilado y despliegue. Se puede ver un [ejemplo 
hecho para BE](doc/build.gradle.example)
- Spock ha sido utilizado porque define un ciclo de pruebas claro (preparación, simulación, verificacion, etc...) pero
tambien porque es groovy y permite meter xml facilmente dentro del código

Estructura
-------------
dentro de la carpeta src se puede encontrar el paquete tools. 
este paquete incluye clases de ayuda para:

- xml : validar xml (groogy tienen capacidades de por si para manejar facilmente xml)
- jms : manejar colas jms simulando legados, tanto clientes como servidores
- bpm : simular al usuario en tareas manuales de amx-bpm

existen ademas varios proyectos de ejemplo (algunos fallarán al no tener el codigo asociado)

- helloSpockSpec : ejemplo básico que no prueba nada
- EmsSpec : ejemplo de trato de colas sencillo, (escribe y lee de una cola)
- bpmTestTrones : ejemplo de prueba de un proceso bpm manual
- becletest : ejemplo de uso de un proyecto de business events con eventos enviados via jms

Como probar 
==============
tips para crear nuevas pruebas:

Bpm
----
Se recomienda partir del ejemplo.

Las tareas manuales envian y reciben la misma estructura de datos, se puede ver un [ejemplo](doc/formDataExample.xml).

La estructura tiene tres partes

1. Datos de entrada
2. Datos de entrada salida
3. Datos de salida

Esta estructura coincide con la definida en el interface de la tarea manual.

Un truco para ver la estructura de entrada facilmente es recogerla con la funcion *closeHumanTask* (pasando null como 
respuesta) e imprimirla por pantalla, aunque los datos pueden salir como vacios si o están inicializados o son solo de 
salida

